import os

from flask import Flask, request, jsonify
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + \
    ':' + os.environ['MONGODB_PASSWORD'] + '@' + \
    os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']
mongo = PyMongo(app)
db = mongo.db

from ccglookup import routes

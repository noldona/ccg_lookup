from flask import request, jsonify

from ccglookup.app import app, db

@app.route('/', methods=['GET'])
def index():
    return "Hello World!"


@app.route('/cards/', methods=['GET'])
def getCards():
    _cards = db.cards.find()

    item = {}
    data = []
    for card in _cards:
        item = {
            'id': str(card['_id']),
            'name': card['name']
        }
        data.append(item)

    return jsonify(
        status=True,
        data=data
    )

@app.route('/cards/', methods=['POST'])
def addCard():
    data = request.json()
    item = {
        'name': data['name']
    }
    db.cards.insert_one(item)

    return jsonify(
        status=True,
        message='Card created!'
    ), 201

@app.route('/transactions/', methods=['GET'])
def getAllTransactions():
    _transactions = db.transactions.find()

    item = {}
    data = []
    for transaction in _transactions:
        item = {
            'id': str(transaction['_id']),
            'body': transaction['body']
        }
        data.append(item)

    return jsonify(
        status=True,
        data=data
    )

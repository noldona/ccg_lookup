import os

from celery import Celery
from pymongo import MongoClient

broker_url = 'ampq://' + os.environ['RABBIT_USERNAME'] + ':' + \
    os.environ['RABBIT_PASSWORD'] + '@' + os.environ['RABBIT_HOSTNAME'] + \
    ':5672/ccg_lookup'
app = Celery('tasks', broker=broker_url, backend='rpc://')
client = MongoClient(os.environ['MONGODB_HOSTNAME'], '27017')
db = client.ccg_lookup

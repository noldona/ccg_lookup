import requests
import json
import logging
logger = logging.getLogger()

from tasks.celery import app, db

@app.task
def getBlocks():
    url = "https://volition-node-beta.pancakehermit.com/blocks/"

    payload = {}
    headers = {}

    response = requests.request("GET", url, headers=headers, data=payload)

    if response.status_code == 200:
        blocks = response.json()

        for block in blocks['blocks']['blocks']:
            for transaction in block['transactions']:
                body = json.loads(transaction['body'])
                db.transactions.insert_one({'body': body})
    else:
        logger.error('Failed to get blocks from blockchain!')
